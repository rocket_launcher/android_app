package org.latisproject.rocketlauncher.adapters;


import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import org.latisproject.rocketlauncher.R;

public class BluetoothDeviceAdapter extends RecyclerView.Adapter<BluetoothDeviceAdapter.BTDViewHolder> {
    private List<BluetoothDevice> mBluetoothDevices;
    private BluetoothDevice mBluetoothDevice;
    private Context mContext;

    public BluetoothDeviceAdapter(Context context,
                                  List<BluetoothDevice> bluetoothDevices) {
        mBluetoothDevices = bluetoothDevices;
        mContext = context;
    }

    public BluetoothDevice getBluetoothDevice() {
        return mBluetoothDevice;
    }

    @Override
    public BluetoothDeviceAdapter.BTDViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bluetooth_device_list, parent, false);
        BTDViewHolder viewHolder = new BTDViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BluetoothDeviceAdapter.BTDViewHolder holder, int position) {
        holder.bindBluetoothDevice(mBluetoothDevices.get(position));
    }

    @Override
    public int getItemCount() {
        return mBluetoothDevices.size();
    }

    public class BTDViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mBTDLabel;
        private String mBTDAddress;

        public BTDViewHolder(View itemView) {
            super(itemView);
            mBTDLabel = (TextView) itemView.findViewById(R.id.btDeviceLabel);
            itemView.setOnClickListener(this);
            mContext = itemView.getContext();
        }

        public void bindBluetoothDevice(BluetoothDevice bluetoothDevice) {
            mBTDAddress = bluetoothDevice.getAddress();
            mBTDLabel.setText(bluetoothDevice.getName() + " - " + mBTDAddress);
        }

        @Override
        public void onClick(View v) {
            RadioButton button = (RadioButton)v.findViewById(R.id.btSelectedRadioButton);
            button.setChecked(true);
            TextView btdLabel = (TextView)v.findViewById(R.id.btDeviceLabel);
            String label = btdLabel.getText().toString();
            for (BluetoothDevice device : mBluetoothDevices) {
                if (label.contains(device.getAddress().toString())) {
                    mBluetoothDevice = device;
                    break;
                }
            }
        }
    }
}
