package org.latisproject.rocketlauncher.ui.activities;

import android.app.Activity;
import android.bluetooth.BluetoothGatt;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import org.latisproject.rocketlauncher.R;
import org.latisproject.rocketlauncher.adapters.SubsystemListAdapter;
import org.latisproject.rocketlauncher.models.CommSubsystem;
import org.latisproject.rocketlauncher.models.LaunchPad;
import org.latisproject.rocketlauncher.models.SafetySubsystem;
import org.latisproject.rocketlauncher.ui.ble.BluetoothLEHelper;

public class RocketLauncherActivity extends Activity {
    private List<String> mSubsystemNames;
    private Map<String, Boolean> mSubsystemStatuses;
    private ProgressBar mProgressBar;
    private ImageView mStatusImageView;

    private LaunchPad mLaunchPad;
    private SafetySubsystem mSafetySys;
    private CommSubsystem mCommSys;

    private SubsystemListAdapter mAdapter;

    private int mConnectionState;
    private int mLaunchState = 0;
    private boolean mEnableLaunch = false;
    private Handler mHandler;

    private int mCurrentTime = 10;
    private int mCountdownFrequency = 1000;

    private BluetoothLEHelper mLaunchPadOps;

    @BindView(android.R.id.list) ListView mListView;
    @BindView(android.R.id.empty) TextView mEmptyTextView;
    @BindView(R.id.launchButton) Button mLaunchButton;
    @BindView(R.id.disarmButton) Button mArmDisarmButton;
    @BindView(R.id.detailActivityLaunchPadLabel) TextView mDetailPadLabel;
    @BindView(R.id.systemsRelativeLayout) RelativeLayout mSystemsLayout;
    @BindView(R.id.countdownTextView) TextView mCountdownText;
    @BindView(R.id.reconnectButton) Button mReconnectButton;

    private static final String TAG = RocketLauncherActivity.class.getSimpleName();


    /*
     Override functions for Android Activity Lifecycles
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rocket_launcher);
        ButterKnife.bind(this);

        // Get the launch pad that was clicked...
        Intent intent = getIntent();
        mLaunchPad = intent.getExtras().getParcelable(MainActivity.PAD_DETAIL);
        mSafetySys = mLaunchPad.getSafetySubsystem();
        mCommSys = mLaunchPad.getCommSys();
        mDetailPadLabel.setText(mLaunchPad.getName());



        // ... set the name and status info to member fields...
        mSubsystemStatuses = new HashMap<>();
        mSubsystemNames = new ArrayList<>();
        mSubsystemStatuses.put(mSafetySys.getName(), mSafetySys.isGoForLaunch());
        mSubsystemNames.add(mSafetySys.getName());
        mSubsystemStatuses.put(mCommSys.getName(), mCommSys.isGoForLaunch());
        mSubsystemNames.add(mCommSys.getName());

        // ... create and assign a new adapter with the subsystem names list...
        mAdapter = new SubsystemListAdapter(this, mSubsystemNames, mSubsystemStatuses, mEnableLaunch);
        mListView.setAdapter(mAdapter);
        mListView.setEmptyView(mEmptyTextView);

        // ... and show the subsystems.
        showSubsystemStatuses();

        // Set Reconnect button
        mReconnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLaunchPadOps.connectController();
            }
        });

        // Set Launch and Abort button functions
        mLaunchButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Log.d(TAG, "Launch button touched");
                        executeLaunchSequence();
                        break;

                    case MotionEvent.ACTION_UP:
                        Log.d(TAG, "Launch button released");
                        abortLaunchSequence();
                        Log.d(TAG, "Launch button release reset: " + mCurrentTime + ", " + mLaunchState);
                        break;
                }
                return false;
            }
        });

        mArmDisarmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleLaunchFunctionality();
            }
        });

        // Register the receiver with which to do the BLE stuff
        mLaunchPadOps = new BluetoothLEHelper(this,mLaunchPad);
        IntentFilter filter = new IntentFilter(BluetoothLEHelper.ACTION_DATA_AVAILABLE);
        filter.addAction(BluetoothLEHelper.ACTION_GATT_CONNECTED);
        filter.addAction(BluetoothLEHelper.ACTION_GATT_DISCONNECTED);
        filter.addAction(BluetoothLEHelper.ACTION_GATT_SERVICES_DISCOVERED);
        LocalBroadcastManager.getInstance(this).registerReceiver(mGattUpdateReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mGattUpdateReceiver);
        mLaunchPadOps.closeConnection();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mLaunchPadOps.closeConnection();
        finish();
    }


    /*
     Local functions
     */
    private void showSubsystemStatuses(Intent intent) {
        // Decode the data in the intent
        String subsystem = "";
        subsystem = intent.getStringExtra(BluetoothLEHelper.EXTRA_SUBSYSTEM);
        if (subsystem != null) {
            if (mConnectionState == BluetoothGatt.STATE_CONNECTED) {
                if (subsystem.equals(getResources().getString(R.string.safety_chan))) {
                    mSafetySys.setGoForLaunch(intent);
                } else if (subsystem.equals(getResources().getString(R.string.launch_abort_chan))) {
                    mLaunchState = mCommSys.decodeLaunchState(intent);
                    Log.d(TAG, "Launch State decoded: " + mLaunchState);
                }
            } else {
                mSafetySys.setGoForLaunch(false);
                mLaunchState = 0;
            }
        }
        mCommSys.setGoForLaunch(intent);

        showSubsystemStatuses();
    }

    private void showSubsystemStatuses() {
        // Populate the subsystem names list and statuses array and update the ListView
        mSubsystemStatuses.put(mSafetySys.getName(), mSafetySys.isGoForLaunch());
        mSubsystemStatuses.put(mCommSys.getName(), mCommSys.isGoForLaunch());
//        mAdapter.setLaunchEnabled(mEnableLaunch);
        mAdapter.notifyDataSetChanged();
    }

    private void toggleLaunchFunctionality() {
        mEnableLaunch = !mEnableLaunch;
        if (mEnableLaunch) {
            mArmDisarmButton.setText("DISARM");
            mLaunchButton.setBackgroundColor(ContextCompat.getColor(this, android.R.color.holo_green_dark));
            mSystemsLayout.setBackgroundColor(ContextCompat.getColor(this, android.R.color.background_dark));
            mDetailPadLabel.setTextColor(ContextCompat.getColor(this, android.R.color.background_light));
            Log.i(TAG, "Launch capability ARMED");
        } else {
            mArmDisarmButton.setText("ARM");
            mLaunchButton.setBackgroundColor(ContextCompat.getColor(this, android.R.color.darker_gray));
            mSystemsLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.background_material_light));
            mDetailPadLabel.setTextColor(ContextCompat.getColor(this, android.R.color.background_dark));
            Log.i(TAG, "Launch capability DISARMED");
        }
        mLaunchButton.setEnabled(mEnableLaunch);
        showSubsystemStatuses();
    }

    private void executeLaunchSequence() {
        if (mEnableLaunch) {
            // Check that the systems are all go.  If so, send Initiate Launch Sequence Command
            if (allSystemsGo()) {
                if (0 == mLaunchState) {
                    mLaunchPadOps.sendInitiateLaunchSequenceCommand();
                    // Seed the launch state before the next read since the countdown will want to
                    // initiate before confirmation comes back.
                    mLaunchState = 1;
                    Log.i(TAG, "Launch sequence initiated. Launch State " + mLaunchState);
                } else {
                    Toast.makeText(this, "Launch aborted", Toast.LENGTH_SHORT).show();
                    abortLaunchSequence();
                    Log.d(TAG, "Launch sequence not initiated on call to executeLaunchSequence().  Launch State " + mLaunchState);
                }

                // While the button is being held down, count down from 10 once per second
                Log.i(TAG, "Countdown begins: T - " + mCurrentTime + " sec, Launch State " + mLaunchState);
                mCountdownText.setVisibility(View.VISIBLE);
                mCountdownText.setText(mCurrentTime + "");
                mCurrentTime--;

                // The delayed posts will handle T-9 through T-0
                mHandler = new Handler();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mCountdownText.setText(mCurrentTime + "");
                        // if 9 > t > 0 & launch state 1: count down
                        if (mCurrentTime > 0 && mLaunchState == 1) {
                            if (allSystemsGo()) {
                                Log.i(TAG, "Countdown: T - " + mCurrentTime + " sec, Launch State " + mLaunchState);
                                mHandler.postDelayed(this, mCountdownFrequency);
                            } else {
                                abortLaunchSequence();
                            }
                        } else if (mCurrentTime == 0 && mLaunchState == 1) {
                            // Perform final status check and launch
                            if (allSystemsGo()) {
                                Log.i(TAG, "All systems go: T - " + mCurrentTime + " sec, Launch State " +
                                        mLaunchState + "  Launching!");
                                mLaunchPadOps.sendLaunchCommand();
                            }
                            abortLaunchSequence();
                            Log.i(TAG, "Reset countdown timer and Launch State.  Ready to relaunch.");
                            return;
                        } else {
                            Toast.makeText(RocketLauncherActivity.this,
                                    getResources().getString(R.string.launch_aborted),
                                    Toast.LENGTH_LONG).show();
                            Log.d(TAG, "mHandler.run() catch-all case: T - " + mCurrentTime + " sec, Launch State " + mLaunchState);
                            abortLaunchSequence();
                            return;
                        }
                        mCurrentTime --;
                    }
                }, mCountdownFrequency);

            } else {
                // Ensure the launch sequence is reset
                abortLaunchSequence();
                Log.d(TAG, "Launch was not enabled but execute function was called: T - " + mCurrentTime + " sec, Launch State " + mLaunchState);
            }
        }
    }

    private boolean allSystemsGo() {
        return mSafetySys.isGoForLaunch() && mCommSys.isGoForLaunch();
    }

    private void abortLaunchSequence() {
        mLaunchPadOps.sendAbortCommand();
        mCurrentTime = 10;
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }
        mCountdownText.setVisibility(View.INVISIBLE);
        if (mEnableLaunch) {
            toggleLaunchFunctionality();
        }
    }

    public static int determineStatusImageId(boolean subsystemStatus) {
        if (subsystemStatus) {
            return android.R.drawable.presence_online;
        }
        else {
            return android.R.drawable.presence_busy;
        }
    }



    /*
     Anonymous inner classes
     */
    // Refer to BLE Helper class for possible actions
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLEHelper.ACTION_GATT_CONNECTED.equals(action)) {
                mConnectionState = BluetoothLEHelper.STATE_CONNECTED;
                mReconnectButton.setVisibility(View.INVISIBLE);
            } else if (BluetoothLEHelper.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnectionState = BluetoothLEHelper.STATE_DISCONNECTED;
                mReconnectButton.setVisibility(View.VISIBLE);
                abortLaunchSequence();
            }
            showSubsystemStatuses(intent);
        }
    };
}
