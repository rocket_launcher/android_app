package org.latisproject.rocketlauncher.models;


import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import org.latisproject.rocketlauncher.ui.ble.BluetoothLEHelper;

public class CommSubsystem extends ControllerSubsystem implements Parcelable {

    public CommSubsystem() {}

    public CommSubsystem(String name) {
        super.setName(name);
    }


    /*
     Abstract methods from ControllerSubsystem
     */
    public void setGoForLaunch(Intent intent) {
        super.setGoForLaunch(decodeMessage(intent));
    }


    /* 
     Override methods from Parcelable
     */
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(super.getName());
        dest.writeByte((byte)(super.isGoForLaunch() ? 1 : 0));
    }


    public CommSubsystem(Parcel in) {
        super.setName(in.readString());
        super.setGoForLaunch(in.readByte() != 0);
    }

    public static final Creator<CommSubsystem> CREATOR = new Creator<CommSubsystem>() {
        @Override
        public CommSubsystem createFromParcel(Parcel in) {
            return new CommSubsystem(in);
        }

        @Override
        public CommSubsystem[] newArray(int size) {
            return new CommSubsystem[size];
        }
    };


    /*
     Local methods
     */
    private boolean decodeMessage(Intent intent) {
        String action = intent.getAction();
        return BluetoothLEHelper.ACTION_DATA_AVAILABLE.equals(action) ||
                BluetoothLEHelper.ACTION_GATT_CONNECTED.equals(action) ||
                BluetoothLEHelper.ACTION_GATT_SERVICES_DISCOVERED.equals(action);
    }

    public int decodeLaunchState(Intent intent) {
        byte[] data = intent.getByteArrayExtra(BluetoothLEHelper.EXTRA_DATA);
//        int launchState = ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN).getInt();
        int launchState = (int) data[0];
        return launchState;
    }
}
