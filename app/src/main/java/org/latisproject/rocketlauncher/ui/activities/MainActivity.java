package org.latisproject.rocketlauncher.ui.activities;

import android.Manifest;
import android.app.DialogFragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import org.latisproject.rocketlauncher.R;
import org.latisproject.rocketlauncher.adapters.LaunchPadAdapter;
import org.latisproject.rocketlauncher.models.LaunchPad;
import org.latisproject.rocketlauncher.ui.App;
import org.latisproject.rocketlauncher.ui.fragments.AddLaunchPadDialogFragment;
import org.latisproject.rocketlauncher.ui.fragments.DeleteLaunchPadsDialogFragment;
import org.latisproject.rocketlauncher.ui.interfaces.NoticeDialogListener;

public class MainActivity extends AppCompatActivity implements NoticeDialogListener {
    private final static int REQUEST_ENABLE_BT = 1;
    private static final int REQUEST_ENABLE_LOCATION = 11;
    private static final int PERMISSIONS_REQUEST_CODE = 3;
    public final static int PARCEL_BLUETOOTH_DEVICE = 2;

    public final static String PAD_DETAIL = "PAD_DETAIL";
    public final static String FRAGMENT_ADD_LP = "FRAGMENT_ADD_LP";
    public final static String FRAGMENT_DELETE_LP = "FRAGMENT_DELETE_LP";
    public final static String BUNDLE_BT_DEVICES = "BUNDLE_BT_DEVICES";
    public static final String BUNDLE_LP_NAMES = "BUNDLE_LP_NAMES";

    private static final String TAG = MainActivity.class.getSimpleName();

    public static boolean BT_PERMISSION_GRANTED = false;

    private List<LaunchPad> mLaunchPads = new ArrayList<>();
    private BluetoothAdapter mBluetoothAdapter;

    private LaunchPadAdapter mAdapter;

    @BindView(R.id.mainRecyclerView) RecyclerView mRecyclerView;
    @BindView(R.id.emptyPadListTextView) TextView mEmptyTextView;


    /*
     Override methods for Android Activity Lifecycles
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // Set the app context so we can use it in the Add LP DialogFragment
        App.setContext(this);

        // Check for Bluetooth capabilities and permissions
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "Bluetooth LE is not supported on this device", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        // If the user has Marshmallow or more recent, request permissions at runtime (if we don't already have them)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            BT_PERMISSION_GRANTED = (checkSelfPermission(Manifest.permission.BLUETOOTH) == PackageManager.PERMISSION_GRANTED) &&
                    (checkSelfPermission(Manifest.permission.BLUETOOTH_ADMIN) == PackageManager.PERMISSION_GRANTED) &&
                    (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);

            if (!BT_PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                }, PERMISSIONS_REQUEST_CODE);
            }
        }
        else {
            BT_PERMISSION_GRANTED = PermissionChecker.checkSelfPermission(this, Manifest.permission.BLUETOOTH) == PermissionChecker.PERMISSION_GRANTED &&
                    PermissionChecker.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN) == PermissionChecker.PERMISSION_GRANTED &&
                    PermissionChecker.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PermissionChecker.PERMISSION_GRANTED;
        }

        // Enable Bluetooth if it isn't enabled
        if (!mBluetoothAdapter.isEnabled()) {
            // If the user has Marshmallow or more recent, request permissions at runtime
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        // Enable location services if they're not enabled
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(10000L)
                .setFastestInterval(5000L);

        LocationSettingsRequest.Builder locationBuilder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
                .setAlwaysShow(true);

        PendingResult<LocationSettingsResult> locationResult = LocationServices.SettingsApi.
                checkLocationSettings(googleApiClient, locationBuilder.build());
        locationResult.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(MainActivity.this, REQUEST_ENABLE_LOCATION);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Toast.makeText(MainActivity.this, "Unable to operate Rocket Launcher without Location Services enabled", Toast.LENGTH_LONG).show();
                        finish();
                        break;
                }
            }
        });

        // Get whatever launch pads are stored on the device and show it
        initializeLaunchPadList();
        mAdapter = new LaunchPadAdapter(this, mLaunchPads);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(layoutManager);
        showLaunchPadList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addLaunchPadMenuButton:
                addLaunchPad(true);
                break;

            case R.id.removeLaunchPadMenuButton:
                removeLaunchPads();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0) {
                BT_PERMISSION_GRANTED = (grantResults[0] == PackageManager.PERMISSION_GRANTED) &&
                        (grantResults[1] == PackageManager.PERMISSION_GRANTED) &&
                        (grantResults[2] == PackageManager.PERMISSION_GRANTED);
            }
            else {
                BT_PERMISSION_GRANTED = false;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                BT_PERMISSION_GRANTED = true;
            } else {
                BT_PERMISSION_GRANTED = false;
            }
        }
        if (requestCode == REQUEST_ENABLE_LOCATION) {
            if (resultCode == RESULT_CANCELED) {
                finish();
            }
        }
    }

    /*
         Override methods from NoticeDialogListener for Fragments
         */
    @Override
    public void onDialogPositiveClick(DialogFragment dialog, List<Integer> selected) {
        for (int ii=selected.size()-1; ii>=0; ii--) {
            mLaunchPads.remove((int)selected.get(ii));
        }
        showLaunchPadList();
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, LaunchPad launchPad) {
        mLaunchPads.add(launchPad);
        showLaunchPadList();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog, int id) {
        // Do nothing
    }


    /*
     Local private methods
     */
    private void addLaunchPad(boolean enable) {
        List<BluetoothDevice> claimedDevices = new ArrayList<>();
        for (LaunchPad pad : mLaunchPads) {
            claimedDevices.add(pad.getBluetoothDevice());
        }
        DialogFragment newFragment = new AddLaunchPadDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(BUNDLE_BT_DEVICES, (ArrayList<? extends Parcelable>) claimedDevices);
        newFragment.setArguments(bundle);
        newFragment.show(getFragmentManager(), FRAGMENT_ADD_LP);
    }

    private void initializeLaunchPadList() {
    /* We will eventually get a list of LaunchPad objects
       from a bluetooth radio library/service/whatever that
       will be persisted in memory or file.  For now, start
       with a blank slate.
     */
    }

    private void showLaunchPadList() {
        if (mLaunchPads.isEmpty()) {
            mEmptyTextView.setVisibility(View.VISIBLE);
        }
        else {
            mEmptyTextView.setVisibility(View.INVISIBLE);
        }
        mAdapter.notifyDataSetChanged();
    }

    private void removeLaunchPads() {
        // Set the list of current LP names
        String[] launchPadNames = new String[mLaunchPads.size()];
        for (int ii=0; ii<mLaunchPads.size(); ii++) {
            launchPadNames[ii] = mLaunchPads.get(ii).getName();
        }

        // Create popup menu for removing LP data
        DialogFragment newFragment = new DeleteLaunchPadsDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArray(BUNDLE_LP_NAMES, launchPadNames);
        newFragment.setArguments(bundle);
        newFragment.show(getFragmentManager(), FRAGMENT_DELETE_LP);
    }
}
