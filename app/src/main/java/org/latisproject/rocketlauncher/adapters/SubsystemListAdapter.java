package org.latisproject.rocketlauncher.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import org.latisproject.rocketlauncher.R;
import org.latisproject.rocketlauncher.ui.activities.RocketLauncherActivity;

public class SubsystemListAdapter extends BaseAdapter {
    private Context mContext;
    private Map<String, Boolean> mControllerSubsystems;
    private List<String> mSubsystemNames;
    private boolean[] mSubsystemStatuses;
    private boolean mLaunchEnabled;

    public SubsystemListAdapter(Context context,
                                List<String> subsystemNames,
                                Map<String, Boolean> controllerSubsystems,
                                boolean launchEnabled) {
        mContext = context;
        mSubsystemNames = subsystemNames;
        mControllerSubsystems = controllerSubsystems;
        mLaunchEnabled = launchEnabled;
    }

    public void setLaunchEnabled(boolean launchEnabled) {
        mLaunchEnabled = launchEnabled;
    }

    @Override
    public int getCount() {
        return mControllerSubsystems.size();
    }

    @Override
    public Object getItem(int position) {
        return mControllerSubsystems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            // brand new
            convertView = LayoutInflater.from(mContext).inflate(R.layout.subsystem_status_list, null);
            holder = new ViewHolder();
            holder.subsystemStatusLabel = (TextView) convertView.findViewById(R.id.subsystemStatusLabel);
            holder.statusImageView = (ImageView) convertView.findViewById(R.id.statusImageView);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        String subsystem = mSubsystemNames.get(position);
        holder.subsystemStatusLabel.setText(subsystem);
        holder.statusImageView.setImageResource(
                RocketLauncherActivity.determineStatusImageId(mControllerSubsystems.get(subsystem))
        );

        return convertView;
    }

    private static class ViewHolder {
        TextView subsystemStatusLabel;
        ImageView statusImageView;
    }
}
