package org.latisproject.rocketlauncher.models;


import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import org.latisproject.rocketlauncher.ui.ble.BluetoothLEHelper;

public class SafetySubsystem extends ControllerSubsystem implements Parcelable {

    public SafetySubsystem() {}

    public SafetySubsystem(String name) {
        super.setName(name);
    }

    /*
     Abstract methods from ControllerSubsystem
      */
    public void setGoForLaunch(Intent intent) {
        super.setGoForLaunch(decodeMessage(intent));
    }

    /*
    Override methods from Parcelable
     */
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(super.getName());
        dest.writeByte((byte)(super.isGoForLaunch() ? 1 : 0));

    }


    public SafetySubsystem(Parcel in) {
        super.setName(in.readString());
        super.setGoForLaunch(in.readByte() != 0);
    }

    public static final Creator<SafetySubsystem> CREATOR = new Creator<SafetySubsystem>() {
        @Override
        public SafetySubsystem createFromParcel(Parcel in) {
            return new SafetySubsystem(in);
        }

        @Override
        public SafetySubsystem[] newArray(int size) {
            return new SafetySubsystem[size];
        }
    };

    /*
    Local methods
     */
    private boolean decodeMessage(Intent intent) {
        byte[] data = intent.getByteArrayExtra(BluetoothLEHelper.EXTRA_DATA);
        return data[0] == 0x01;
    }
}
