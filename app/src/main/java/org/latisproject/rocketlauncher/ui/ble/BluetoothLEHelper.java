package org.latisproject.rocketlauncher.ui.ble;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import org.latisproject.rocketlauncher.R;
import org.latisproject.rocketlauncher.models.LaunchPad;

public class BluetoothLEHelper {
    private static final String TAG = BluetoothLEHelper.class.getSimpleName();
    private Context mContext;

    private BluetoothGatt mBluetoothGatt;
    private List<BluetoothGattService> mGattServices;
    private Map<UUID,List<BluetoothGattCharacteristic>> mGattCharacteristics;
    private int mConnectionState = STATE_DISCONNECTED;
    private BluetoothDevice mDevice;

    public static final int STATE_DISCONNECTED = 0;
    public static final int STATE_CONNECTED = 2;

    private boolean mWriteSuccess = false;

    public final static String ACTION_GATT_CONNECTED = "ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED = "ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED = "ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE = "ACTION_DATA_AVAILABLE";
    public final static String EXTRA_SUBSYSTEM = "EXTRA_SUBSYSTEM";
    public final static String EXTRA_DATA = "EXTRA_DATA";

    private final static UUID UUID_LAUNCH_ABORT = UUID.fromString("66A8E213-68A4-4699-8B7A-C351EE01668F");
    private final static UUID UUID_SAFETY = UUID.fromString("FD3B44CE-22B6-4114-9E0D-AC0272396465");
    private final static UUID[] uuidArray = {UUID_LAUNCH_ABORT, UUID_SAFETY};

    public BluetoothLEHelper(Context context, LaunchPad launchPad) {
        mContext = context;

        // Start the BLE adapter
        BluetoothManager bluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();

        mDevice = launchPad.getBluetoothDevice();
        connectController();
    }


    /*
     Local public methods
     */
    public void sendAbortCommand() {
        if (mConnectionState == STATE_CONNECTED) {
            byte[] mssg = {0x00};
            writeToCharacteristic(UUID_LAUNCH_ABORT, mssg);
        }
    }

    public void sendInitiateLaunchSequenceCommand() {
        if (mConnectionState == STATE_CONNECTED) {
            byte[] mssg = {0x01};
            writeToCharacteristic(UUID_LAUNCH_ABORT, mssg);
        }
    }

    public void sendLaunchCommand() {
        if (mConnectionState == STATE_CONNECTED) {
            byte[] mssg = {0x02};
            writeToCharacteristic(UUID_LAUNCH_ABORT, mssg);
        }
    }

    public void closeConnection() {
        if (mBluetoothGatt != null) {
            mBluetoothGatt.close();
            mBluetoothGatt = null;
        }
        mConnectionState = STATE_DISCONNECTED;
    }


    /*
     Local private methods
     */
    private void readCharacteristicPeriodically(final BluetoothGattCharacteristic characteristic, int delay) {
        int pollTime = 1000; // ms
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (mConnectionState == STATE_CONNECTED) {
                    mBluetoothGatt.readCharacteristic(characteristic);
                } else {
                    cancel();
                }
            }
        }, delay, pollTime);
    }

    private void writeToCharacteristic(final UUID serviceUuid, byte[] b) {
        List<BluetoothGattCharacteristic> characteristics = mGattCharacteristics.get(serviceUuid);
        BluetoothGattCharacteristic characteristic = null;
        for (int cc=0; cc<characteristics.size(); cc++) {
            characteristic = characteristics.get(cc);
            if (characteristic.getUuid().toString().equals(serviceUuid.toString())) {
                break;
            }
        }
        if (characteristic != null) {
            characteristic.setValue(b);
            boolean successfullyStarted = false;
            do {
                successfullyStarted = mBluetoothGatt.writeCharacteristic(characteristic);
            } while (!successfullyStarted);
            if (successfullyStarted) {
                Log.d(TAG, "BLE write operation started successfully");
            }
        } else {
            Toast.makeText(mContext, "Unable to read or write to signal: " + getSubsystem(serviceUuid), Toast.LENGTH_LONG).show();
        }
    }

    private String getSubsystem(UUID characteristicUuid) {
        String subsystem = "";
        if (characteristicUuid.toString().equals(UUID_LAUNCH_ABORT.toString())) {
            subsystem = mContext.getResources().getString(R.string.launch_abort_chan);
        }
        else if (characteristicUuid.toString().equals(UUID_SAFETY.toString())) {
            subsystem = mContext.getResources().getString(R.string.safety_chan);
        } else {
            subsystem = "Unknown characteristic " + characteristicUuid.toString();
        }
        return subsystem;
    }

    private void broadcastUpdate(String action) {
        Intent localIntent = new Intent(action);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(localIntent);
    }

    private void broadcastUpdate(String action, BluetoothGattCharacteristic characteristic) {
        // Get the subsystem and the data
        String subsystem = getSubsystem(characteristic.getUuid());
        byte[] data = characteristic.getValue();

        // Put the subsystem name and data into an intent and send it
        final Intent localIntent = new Intent(action);
        localIntent.putExtra(EXTRA_SUBSYSTEM, subsystem);
        localIntent.putExtra(EXTRA_DATA, data);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(localIntent);
    }

    public void connectController() {
        if (mDevice != null) {
            mBluetoothGatt = mDevice.connectGatt(mContext, false, mGattCallback);
        }
    }


    /*
         Anonymous inner classes
         */
    private final BluetoothGattCallback mGattCallback =
            new BluetoothGattCallback() {
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                                    int newState) {
                    String intentAction;
                    if (newState == BluetoothProfile.STATE_CONNECTED) {
                        intentAction = ACTION_GATT_CONNECTED;
                        mConnectionState = STATE_CONNECTED;
                        broadcastUpdate(intentAction);
                        Log.i(TAG, "Connected to GATT server.");
                        mBluetoothGatt.discoverServices();
                    } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                        intentAction = ACTION_GATT_DISCONNECTED;
                        mConnectionState = STATE_DISCONNECTED;
                        Log.i(TAG, "Disconnected from GATT server.");
                        broadcastUpdate(intentAction);
                    }
                }

                @Override
                // New services discovered
                public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        // Build a map of the characteristics for each service
                        mGattCharacteristics = new HashMap<>();
                        mGattServices = new ArrayList<>();
                        int delay = 0;
                        for (UUID uuid : uuidArray) {
                            // Get the service for this uuid and add it to the list
                            BluetoothGattService service = gatt.getService(uuid);
                            if (service == null) {
                                continue;
                            }
                            mGattServices.add(service);

                            // Get the characteristics for this service, add them to the list,
                            // and set up notifications
                            List<BluetoothGattCharacteristic> characteristics = new ArrayList<>();
                            characteristics.addAll(service.getCharacteristics());
                            mGattCharacteristics.put(uuid,characteristics);
                            for (BluetoothGattCharacteristic characteristic : characteristics) {
                                if (characteristic.getUuid().toString().equals(uuid.toString())) {
                                    // Stagger the reading by a quarter second to ensure that
                                    // measurements don't get dropped on the floor
                                    readCharacteristicPeriodically(characteristic, delay);
                                }
                            }
                            delay += 250;
                        }
                        broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
                    } else {
                        Log.w(TAG, "onServicesDiscovered received: " + status);
                    }
                }

                @Override
                // Result of a characteristic read operation
                public void onCharacteristicRead(BluetoothGatt gatt,
                                                 BluetoothGattCharacteristic characteristic,
                                                 int status) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        broadcastUpdate(ACTION_DATA_AVAILABLE,characteristic);
                    }
                }

                @Override
                public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        Log.d(TAG, "BLE write operation completed successfully");
                    } else {
                        Log.d(TAG, "BLE write operation FAILURE");
                    }
                }

                //                @Override
//                public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
//                    decodeMessage(characteristic);
//                }
            };

}