package org.latisproject.rocketlauncher.ui.fragments;


import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import org.latisproject.rocketlauncher.R;
import org.latisproject.rocketlauncher.ui.activities.MainActivity;
import org.latisproject.rocketlauncher.ui.interfaces.NoticeDialogListener;

public class DeleteLaunchPadsDialogFragment extends DialogFragment {
    private View mView;
    private List<Integer> mSelected;

    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) getActivity();
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Get the array of launch pad names
        String[] launchPadNames = getArguments().getStringArray(MainActivity.BUNDLE_LP_NAMES);

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        mSelected = new ArrayList<>();
        builder.setTitle(R.string.delete_lp_title)
                .setMultiChoiceItems(launchPadNames, null, new DialogInterface.OnMultiChoiceClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        if (isChecked) {
                            mSelected.add(which);
                        }
                        else if (mSelected.contains(which)) {
                            mSelected.remove(Integer.valueOf(which));
                        }
                    }
                })
                .setPositiveButton(R.string.delete_lp_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User deleted some LPs
                        mListener.onDialogPositiveClick(DeleteLaunchPadsDialogFragment.this, mSelected);
                    }
                })
                .setNegativeButton(R.string.cancel_string, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        mListener.onDialogNegativeClick(DeleteLaunchPadsDialogFragment.this, id);
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
