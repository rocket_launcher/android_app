package org.latisproject.rocketlauncher.ui.fragments;


import android.app.Dialog;
import android.app.DialogFragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import org.latisproject.rocketlauncher.R;
import org.latisproject.rocketlauncher.adapters.BluetoothDeviceAdapter;
import org.latisproject.rocketlauncher.models.LaunchPad;
import org.latisproject.rocketlauncher.ui.App;
import org.latisproject.rocketlauncher.ui.activities.MainActivity;
import org.latisproject.rocketlauncher.ui.interfaces.NoticeDialogListener;

public class AddLaunchPadDialogFragment extends DialogFragment {
    private BluetoothDeviceAdapter mBTDAdapter;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mBluetoothLeScanner;
    private Handler mHandler;
    private View mView;
    private Context mContext;
    private List<BluetoothDevice> mDevices;
    private List<BluetoothDevice> mClaimedDevices;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private TextView mBtHeader;

    private final static String TAG = AddLaunchPadDialogFragment.class.getSimpleName();
    private final static String BT_SEARCHING = "Searching...";
    private final static String BT_DISPLAY = "Available Bluetooth Devices:";
    private final static int SCAN_PERIOD = 10000;


    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) getActivity();
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mDevices = new ArrayList<>();
        mClaimedDevices = new ArrayList<>();
        mClaimedDevices = getArguments().getParcelableArrayList(MainActivity.BUNDLE_BT_DEVICES);
        mContext = App.getContext();

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        mView = inflater.inflate(R.layout.add_launch_pad_dialog, null);
        mRecyclerView = (RecyclerView) mView.findViewById(R.id.addLPRecyclerView);
        mProgressBar = (ProgressBar) mView.findViewById(R.id.addLpProgressBar);
        mBtHeader = (TextView) mView.findViewById(R.id.availBtHeader);
        toggleBtHeader();

        // Get the list of BLE devices and show it
        mHandler = new Handler();
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getActivity().getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        scanLeDevice(MainActivity.BT_PERMISSION_GRANTED);

        // Set the RecyclerView adapter for the list of bluetooth devices
        mBTDAdapter = new BluetoothDeviceAdapter(mContext, mDevices);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setAdapter(mBTDAdapter);
        mRecyclerView.setLayoutManager(layoutManager);
        showBluetoothDevices();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(mView)
                .setMessage(R.string.add_lp_dialog_string)
                .setPositiveButton(R.string.add_string, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User added a LP
                        TextView nlpView = (TextView) mView.findViewById(R.id.newLaunchPadLabel);
                        String newLaunchPadName = nlpView.getText().toString();
                        BluetoothDevice device = mBTDAdapter.getBluetoothDevice();
                        LaunchPad launchPad = new LaunchPad(newLaunchPadName);
                        launchPad.setBluetoothDevice(device);

                        mListener.onDialogPositiveClick(AddLaunchPadDialogFragment.this, launchPad);
                    }
                })
                .setNegativeButton(R.string.cancel_string, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        mListener.onDialogNegativeClick(AddLaunchPadDialogFragment.this, id);
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
            scanLeDevice(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        showBluetoothDevices();
    }


    /*
     Local private methods
     */
    private void toggleBtHeader() {
        if (mProgressBar.getVisibility() == View.VISIBLE) {
            mProgressBar.setVisibility(View.INVISIBLE);
            mBtHeader.setText(BT_DISPLAY);
        }
        else {
            mProgressBar.setVisibility(View.VISIBLE);
            mBtHeader.setText(BT_SEARCHING);
        }
    }

    private void scanLeDevice(boolean enable) {
        if (enable) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBluetoothLeScanner.stopScan(mScanCallback);
                    toggleBtHeader();
                    showBluetoothDevices();
                }
            }, SCAN_PERIOD);
            mBluetoothLeScanner.startScan(mScanCallback);
            toggleBtHeader();

        } else {
            mBluetoothLeScanner.stopScan(mScanCallback);
            if (mProgressBar.getVisibility() == View.VISIBLE) {
                toggleBtHeader();
            }
        }
    }

    private void showBluetoothDevices() {
        mBTDAdapter.notifyDataSetChanged();
    }


    /*
     Anonymous inner classes
     */
    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            BluetoothDevice device = result.getDevice();
            if (!mDevices.contains(device) && !mClaimedDevices.contains(device)) {
                mDevices.add(device);
            }
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            for (ScanResult sr : results) {
                if (!mDevices.contains(sr.getDevice()) && !mClaimedDevices.contains(sr.getDevice())) {
                    mDevices.add(sr.getDevice());
                }
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            Toast.makeText(App.getContext(),"Failed to scan for BLE devices", Toast.LENGTH_LONG).show();
        }
    };

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi,
                             byte[] scanRecord) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!mDevices.contains(device) && !mClaimedDevices.contains(device)) {
                        mDevices.add(device);
                    }
                }
            });
        }
    };
}
