package org.latisproject.rocketlauncher.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import org.latisproject.rocketlauncher.R;
import org.latisproject.rocketlauncher.models.LaunchPad;
import org.latisproject.rocketlauncher.ui.App;
import org.latisproject.rocketlauncher.ui.activities.MainActivity;
import org.latisproject.rocketlauncher.ui.activities.RocketLauncherActivity;


public class LaunchPadAdapter extends RecyclerView.Adapter<LaunchPadAdapter.LaunchPadViewHolder> {
    private List<LaunchPad> mLaunchPads;
    private Context mContext;

    public LaunchPadAdapter(Context context, List<LaunchPad> launchPads) {
        mContext = context;
        mLaunchPads = launchPads;
    }

    @Override
    public LaunchPadViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.launch_pad_list, parent, false);
        return new LaunchPadViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LaunchPadViewHolder holder, int position) {
        LaunchPad launchPad = mLaunchPads.get(position);
        // if this device is connected, show black icon.  Else, show grey.
        if (launchPad.getBluetoothDevice() != null) {
            holder.mBtImage.setColorFilter(ContextCompat.getColor(mContext, android.R.color.black), PorterDuff.Mode.MULTIPLY);
        } else {
            holder.mBtImage.setColorFilter(ContextCompat.getColor(mContext, android.R.color.background_light), PorterDuff.Mode.MULTIPLY);
        }
        holder.bindLaunchPad(launchPad);
    }

    @Override
    public int getItemCount() {
        return mLaunchPads.size();
    }


    public class LaunchPadViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        public TextView mLaunchPadLabel;
        public ImageView mBtImage;

        public LaunchPadViewHolder(View itemView) {
            super(itemView);
            mLaunchPadLabel = (TextView)itemView.findViewById(R.id.launchPadLabel);
            mBtImage = (ImageView)itemView.findViewById(R.id.btConnImageView);
            itemView.setOnClickListener(this);
            mContext = itemView.getContext();

        }

        public void bindLaunchPad(LaunchPad launchPad) {
            mLaunchPadLabel.setText(launchPad.getName());
        }

        @Override
        public void onClick(View v) {
            // Figure out which launch pad was just clicked
            TextView thisPadView = (TextView) v.findViewById(R.id.launchPadLabel);
            String thisPadLabel = thisPadView.getText().toString();
            LaunchPad launchPad = null;
            for (LaunchPad pad : mLaunchPads) {
                if (pad.getName().equals(thisPadLabel)) {
                    launchPad = pad;
                    break;
                }
            }
            if (launchPad == null) {
                Toast.makeText(App.getContext(),
                        String.format("Unable to Launch from Pad %s",thisPadLabel),
                        Toast.LENGTH_LONG).show();
                return;
            }

            // put the content for this launch pad in an intent
            Intent intent = new Intent(mContext, RocketLauncherActivity.class);
            intent.putExtra(MainActivity.PAD_DETAIL, launchPad);

            // start the launch pad detail activity
            mContext.startActivity(intent);
        }
    }
}
