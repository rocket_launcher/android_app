# Download the app directly from the repository
- Latest stable version (download this one): [android app](https://gitlab.com/rocket_launcher/android_app/builds/artifacts/master/raw/app/build/outputs/apk/app-debug.apk?job=build)
- Current development/bleeding edge version (*don't download this one*, unless you're ready for things to break unexpectedly): [android app](https://gitlab.com/rocket_launcher/android_app/builds/artifacts/develop/raw/app/build/outputs/apk/app-debug.apk?job=build)

# Download the app from Google Play Store
Coming soon

# Current build status
| Master | Develop |
|:-:|:-:|
| ![build](https://gitlab.com/rocket_launcher/android_app/badges/master/build.svg) | ![build](https://gitlab.com/rocket_launcher/android_app/badges/develop/build.svg) |
