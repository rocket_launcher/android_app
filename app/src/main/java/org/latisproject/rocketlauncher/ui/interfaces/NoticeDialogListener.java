package org.latisproject.rocketlauncher.ui.interfaces;


import android.app.DialogFragment;

import java.util.List;

import org.latisproject.rocketlauncher.models.LaunchPad;

public interface NoticeDialogListener {
    public void onDialogPositiveClick(DialogFragment dialog, List<Integer> selected);
    public void onDialogPositiveClick(DialogFragment dialog, LaunchPad launchPad);
    public void onDialogNegativeClick(DialogFragment dialog, int id);
}
