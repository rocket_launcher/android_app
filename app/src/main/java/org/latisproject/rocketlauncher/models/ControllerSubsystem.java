package org.latisproject.rocketlauncher.models;


import android.content.Intent;

public abstract class ControllerSubsystem {
    private String mName;
    private boolean mIsGoForLaunch = false;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public boolean isGoForLaunch() {
        return mIsGoForLaunch;
    }

    public void setGoForLaunch(boolean goForLaunch) {
        mIsGoForLaunch = goForLaunch;
    }


    /*
     Abstract methods
     */
    public abstract void setGoForLaunch(Intent intent);

}
