package org.latisproject.rocketlauncher.models;


import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;

import org.latisproject.rocketlauncher.R;
import org.latisproject.rocketlauncher.ui.App;
import org.latisproject.rocketlauncher.ui.activities.MainActivity;

public class LaunchPad implements Parcelable {
    public static final int PARCEL_SAFETY   = 1000;
    public static final int PARCEL_POWER    = 1001;
    public static final int PARCEL_IGNITION = 1002;
    public static final int PARCEL_COMMS    = 1003;

    private int mId;
    private String mName = "";
    private SafetySubsystem mSafetySys;
    private CommSubsystem mCommSys;
    private BluetoothDevice mBluetoothDevice;

    public LaunchPad() {
        initializeSubsystems();
    }

    public LaunchPad(String name) {
        initializeSubsystems();
        mName = name;
    }

    private void initializeSubsystems() {
        mSafetySys = new SafetySubsystem(App.getContext()
                .getResources()
                .getString(R.string.safety_chan));
        mCommSys = new CommSubsystem(App.getContext()
                .getResources()
                .getString(R.string.comm_sys));
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public SafetySubsystem getSafetySubsystem() {
        return mSafetySys;
    }

    public void setSafetySubsystem(SafetySubsystem safetySys) {
        mSafetySys = safetySys;
    }

    public CommSubsystem getCommSys() {
        return mCommSys;
    }

    public void setCommSys(CommSubsystem commSys) {
        mCommSys = commSys;
    }

    public BluetoothDevice getBluetoothDevice() {
        return mBluetoothDevice;
    }

    public void setBluetoothDevice(BluetoothDevice bluetoothDevice) {
        mBluetoothDevice = bluetoothDevice;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mName);
        dest.writeParcelable(mSafetySys, PARCEL_SAFETY);
        dest.writeParcelable(mCommSys, PARCEL_COMMS);
        dest.writeParcelable(mBluetoothDevice, MainActivity.PARCEL_BLUETOOTH_DEVICE);
    }

    public LaunchPad(Parcel in) {
        mId = in.readInt();
        mName = in.readString();
        mSafetySys = in.readParcelable(SafetySubsystem.class.getClassLoader());
        mCommSys = in.readParcelable(CommSubsystem.class.getClassLoader());
        mBluetoothDevice = in.readParcelable(BluetoothDevice.class.getClassLoader());
    }

    public static final Creator<LaunchPad> CREATOR = new Creator<LaunchPad>() {
        @Override
        public LaunchPad createFromParcel(Parcel source) {
            return new LaunchPad(source);
        }

        @Override
        public LaunchPad[] newArray(int size) {
            return new LaunchPad[size];
        }
    };
}
